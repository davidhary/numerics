﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Meta.Numerics;
using Meta.Numerics.Functions;
using Meta.Numerics.Analysis;

namespace Test {

    public partial class AdvancedMathTest_Hypergeometric {

        [TestMethod]
        public void HypergeometricCubicTransforms() {

            foreach (double a in abcs) {
                foreach (double x in xs) {

                    // DLMF 15.8.31
                    if ((a == 7.0) && (x == 0.8)) continue;
                    if ((a == -3.0) && (x == 0.8)) continue;
                    if ((a == 3.1) && (x == -5.0)) continue;
                    if ((a == 4.5) && (x == -5.0)) continue;
                    if ((a == 7.0) && (x == -5.0)) continue;
                    if (x < 8.0 / 9.0) {
                        Assert.IsTrue(TestUtilities.IsNearlyEqual(
                            AdvancedMath.Hypergeometric2F1(3.0 * a, 3.0 * a + 0.5, 4.0 * a + 2.0 / 3.0, x),
                            AdvancedMath.Hypergeometric2F1(a, a + 0.5, 2.0 * a + 5.0 / 6.0, 27.0 * x * x * (x - 1.0) / MoreMath.Sqr(9.0 * x - 8.0)) * Math.Pow(1.0 - 9.0 / 8.0 * x, -2.0 * a)
                        ), $"a={a} x={x}");
                    }

                }
            }

        }
    }

}

#if false

        // [TestMethod]
        public void SmokeTest2 () {

        public void ThomsonLocal () {
            // for (int n = 2; n < 8; n++)
            for (int n = 2; n < 6; n++)

            e = Math.Round(e, 9); // DH
            Debug.Assert(Math.Abs(e) <= 0.5);
#endif