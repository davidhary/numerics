﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("Numerics Examples")]
[assembly: AssemblyProduct("Meta.Numerics.Examples")]
[assembly: AssemblyDescription("Meta Numerics Examples")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
