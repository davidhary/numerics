## ISR Automata Finite<sub>&trade;</sub>: Finite State Machine Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.0.8 06/05/2019*  
Packaged as a .Net framework solution. Fixes unit tests and examples 9see change.text or partial classes).

*4.1.4 08/27/2020*  
#### More Extended Precision Types:

We have made multiple improvements to the quadruple-precision (~32 decimal digit)
Double-Double floating point type. It now handles infinities and NaNs appropriately.
Its string rendering and parsing are improved. We have also added more Double-Double
functions, including trig functions and LogGamma. We have added the 128-bit integer types 
Int128 and UInt128. These behave like the built-in integer types Int64 and UInt64 
(long and ulong), but support integer values up to ~10<sup>38</sup>. Arithmetic using 
these types is 1-4 times faster than using BigInteger, and unlike BigInteger, they behave 
like the other fixed-width register types with respect to overflow.  
*More Advanced Functions:*  
We have added a few more advanced functions. 
These include the complete elliptic integral of the third kind and computation 
of the elliptic nome (so now only the incomplete elliptic integral of the third kind
remains unimplemented). We also added a scaled version of the incomplete Bessel 
function (allowing you to work with the function for arguments where the function value itself
 would over- or under-flow), functions that return the zeros of the Airy and Bessel functions, 
and the hyperbolic integral functions Cin and Shi. We have also made many improvements to the 
internals of long-implemented functions to improve their speed, accuracy, 
and behavior at extreme arguments including infinities and NaN.  
 
#### Other Improvements:

We have added the RegressionResult type with exposes residuals and the sum of squared residuals 
on all FitResults that have them. We fixed a bug which could cause non-convergence in the 
multi-dimensional FindLocalMaximum and FindLocalMinimum methods.   

\(C\) 2008 David Wright. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Meta Numerics](http://www.meta-numerics.net.)
