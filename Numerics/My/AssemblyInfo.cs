﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("Meta Numerics Library")]
[assembly: AssemblyProduct("Meta.Numerics.Library")]
[assembly: AssemblyDescription("A library for numeric computing with support for data manipulation, statistics, matrix algebra, and advanced functions")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
