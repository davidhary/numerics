﻿using System.Reflection;
using System.Resources;
[assembly: AssemblyCompany("Meta Numerics")]
[assembly: AssemblyCopyright("Copyright © David Wright 2008-2018")]
[assembly: AssemblyTrademark("Licensed under The MIT License")]
[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("4.1.4")]

